function borg_archive {
  printf "${GREEN}Adding local mirror to borg repository...${NORMAL}\n"
  $BORG create -v --list --chunker-params=10,23,16,4095 --compression zlib,9 \
      "$BORG_REPOSITORY::$MC_VERSION-$DATE" $LOCAL_MIRROR >> $LOG_FILE 2>&1 &
  progress_spinner "Adding"
  detect_error $?
  printf "${GREEN}\rMirror successfully archived.${NORMAL}\n"
}
function borg_export_log {
  printf "${GREEN}Recording snapshot in log file...${NORMAL}\n"
  $BORG info "$BORG_REPOSITORY::$MC_VERSION-$DATE" >> $LOG_FILE 2>&1 &
  progress_spinner "Exporting"
  detect_error $?
  printf "${GREEN}\rData successfully exported to the log file.${NORMAL}\n"
}
function change_directory {
  cd $GIT_DIRECTORY
}
function check_configuration {
  if [ -z "$1" ]; then
    printf >&2 "%40s\n" "${RED}Error!${NORMAL}"
    printf >&2 "You need to properly configure the $2 variable to use\n"
    printf >&2 "this script. Please modify the script using a text editor of\n"
    printf >&2 "your choice and configure this properly, then execute the\n"
    printf >&2 "script again.\n"
    exit_error
  fi
}

function check_program {
    if [ -z "$1" ]; then
      printf >&2 "%40s\n" "${RED}Error!${NORMAL}"
      printf >&2 "This script requires the following program to be installed:\n"
      printf >&2 "%s\n" "${RED}${UNDERLINE}${1}${NORMAL}"
      printf >&2 "Please make sure it is installed or on your PATH."
      exit_error
    fi
}
function create_logfile {
  printf "${GREEN}Creating log file...${NORMAL}\n"
  printf "Collegi Backup Script\n"                              >> $LOG_FILE
  printf "Version:              $VERSION\n"                     >> $LOG_FILE
  printf "Date and Time:        $(date)\n\n"                    >> $LOG_FILE
  printf "Local Directory:      $LOCAL_MIRROR\n"                >> $LOG_FILE
  printf "Remote Directory:     $REMOTE_DIRECTORY\n"            >> $LOG_FILE
  printf "Minecraft Version:    $MC_VERSION\n"                  >> $LOG_FILE
  printf "LFTP Bookmark:        $LFTP_BOOKMARK\n"               >> $LOG_FILE
  printf "Borg Repository:      $BORG_REPOSITORY\n"             >> $LOG_FILE
  printf "Log Repository:       $LOG_REPOSITORY\n"              >> $LOG_FILE
  printf "Metadata Remote Name: $METADATA_REMOTE_NAME\n"        >> $LOG_FILE
  printf "Data Remove Name:     $DATA_REMOTE_NAME\n\n"          >> $LOG_FILE
  printf "BorgBackup Binary:    $BORG\n"                        >> $LOG_FILE
  printf "Git Binary:           $GIT\n"                         >> $LOG_FILE
  printf "Git-Annex Binary:     $GIT_ANNEX\n"                   >> $LOG_FILE
  printf "LFTP Binary:          $LFTP\n\n"                      >> $LOG_FILE
  $LFTP --version                                               >> $LOG_FILE
  $GIT --version                                                >> $LOG_FILE
  $GIT_ANNEX version                                            >> $LOG_FILE
  $BORG --version                                               >> $LOG_FILE
  printf "${GREEN}Created Log File.${NORMAL}\n"
}
function detect_error {
  if [ $1 -ne 0 ]; then
    printf "%s\n" "${RED}The previous command failed. Check log file.${NORMAL}"
    exit_error
  fi
}
function download_remote {
  printf "${GREEN}Downloading from remote to local mirror...${NORMAL}\n"
  $LFTP "$LFTP_BOOKMARK" -e "mirror -nvpe -P 5 $REMOTE_DIRECTORY \
         $LOCAL_MIRROR" >> $LOG_FILE 2>&1 &
  progress_spinner "Downloading"
  detect_error $?
  printf "${GREEN}\rDownload successfully completed.${NORMAL}\n"
}

function git_annex_add {
  printf "${GREEN}Adding new Repository Data to git-annex...${NORMAL}\n"
  $GIT_ANNEX add collegi.repo >> $LOG_FILE 2>&1 &
  progress_spinner "Adding"
  detect_error $?
  printf "${GREEN}\rRepository Data successfully added to git-annex.${NORMAL}\n"
}
function git_annex_commit {
  printf "${GREEN}Commiting new data to git...${NORMAL}\n"
  $GIT add $LOG_REPOSITORY
  $GIT commit -a -m "Automated Commit: $MC_VERSION-$DATE" >> $LOG_FILE 2>&1 &
  progress_spinner "Committing"
  detect_error $?
  printf "${GREEN}\rCommit completed successfully.${NORMAL}\n"
}

function upload_data {
  printf "${GREEN}Uploading Data to Offsite Storage...${NORMAL}\n"
  $GIT_ANNEX sync --content $DATA_REMOTE_NAME >> $LOG_FILE 2>&1 &
  progress_spinner "Uploading"
  detect_error $?
  printf "${GREEN}\rData successfully uploaded.${NORMAL}\n"
}
function upload_metadata {
  printf "${GREEN}Uploading Metadata to Offsite Storage...${NORMAL}\n"
  $GIT_ANNEX sync $METADATA_REMOTE_NAME >> $LOG_FILE 2>&1 &
  progress_spinner "Uploading"
  detect_error $?
  printf "${GREEN}\rMetadata successfully uploaded.${NORMAL}\n"
}

#
# Main Script
#
printf "${GREEN}Collegi Pixelmon Server - Backup Routine${NORMAL}\n"
printf "${GREEN}Version: $VERSION${NORMAL}\n"
printf "${GREEN}Checking configuration...${NORMAL}\n"
check_configuration "$LOCAL_MIRROR"         "LOCAL_MIRROR"
check_configuration "$MC_VERSION"           "MC_VERSION"
check_configuration "$REMOTE_DIRECTORY"     "REMOTE_DIRECTORY"
check_configuration "$LFTP_BOOKMARK"        "LFTP_BOOKMARK"
check_configuration "$BORG_REPOSITORY"      "BORG_REPOSITORY"
check_configuration "$BORG_PASSPHRASE"      "BORG_PASSPHRASE"
check_configuration "$LOG_REPOSITORY"       "LOG_REPOSITORY"
check_configuration "$METADATA_REMOTE_NAME" "METADATA_REMOTE_NAME"
check_configuration "$DATA_REMOTE_NAME"     "DATA_REMOTE_NAME"
check_configuration "$GIT_DIRECTORY"        "GIT_DIRECTORY"
printf "${GREEN}Checking directories...${NORMAL}\n"
check_directory     "$LOCAL_MIRROR"
check_directory     "$BORG_REPOSITORY"
check_directory     "$LOG_REPOSITORY"
check_directory     "$GIT_DIRECTORY"
printf "${GREEN}Checking if programs installed...${NORMAL}\n"
check_program       $BORG
check_program       $GIT
check_program       $GIT_ANNEX
check_program       $GIT_ANNEX_REMOTE_RCLONE
check_program       $LFTP
check_program       $RCLONE
create_logfile
download_remote
borg_archive
borg_export_log
change_directory
git_annex_add
git_annex_commit
upload_data
upload_metadata
